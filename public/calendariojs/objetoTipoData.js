// let TIPO_DATA;
let TIPO_DATA_ESCOPO_FECHADO;
// export default TIPO_DATA = {
export default TIPO_DATA_ESCOPO_FECHADO = {
    diaMarcado: {
        afastamento: {
            classeCss: "tdDiaAfastamento",
            classeCssDestaque: "tdDiaAfastamentoDestaque",
            classeCssInicio: "tdDiaInicioAfastamento",
            divRelatorioPai: "divDatasAfast",
            divRelatorio: "dataAfast",
            arrayIdDias: [],
            paresDeDatas: [],
            idContornoInicial: null,
        },
        substituicao: {
            classeCss: "tdDiaSubstituicao",
            classeCssDestaque: "tdDiaSubstituicaoDestaque",
            classeCssInicio: "tdDiaInicioSubstituicao",
            divRelatorioPai: "divDatasSubst",
            divRelatorio: "dataSubst",
            arrayIdDias: [],
            paresDeDatas: [],
            idContornoInicial: null,
        },
        conflito: {
            classeCss: "tdDiaEmConflito",
            classeCssDestaque: "tdDiaEmConflitoDestaque",
            classeCssInicio: "",
            divRelatorioPai: "divDatasConflito",
            divRelatorio: "dataConflito",
            arrayIdDias: [],
            paresDeDatas: [],
            idContornoInicial: null,
        },
    },

    diaLimpo: {
        classeCss: "tdDiaLimpo"
    },
};

