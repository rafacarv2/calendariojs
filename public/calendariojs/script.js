import TIPO_DATA_ESCOPO_FECHADO from "./objetoTipoData.js";

window.TIPO_DATA = TIPO_DATA_ESCOPO_FECHADO
const MILISEGUNDOS_EM_UM_DIA = 86400000


document.addEventListener("DOMContentLoaded", function () {
    var tabelasContainer = document.getElementById("tabelasContainer");

    // Função para gerar o cabeçalho da tabela
    function gerarCabecalho(table) {
        var diasDaSemana = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
        var tr = document.createElement('tr');
        for (var i = 0; i < diasDaSemana.length; i++) {
            var th = document.createElement('th');
            th.textContent = diasDaSemana[i];
            tr.appendChild(th);
        }
        table.appendChild(tr);
    }

    // Função para gerar o corpo da tabela para um mês e ano
    function gerarCorpo(tabela, mes, ano) {
        let contadorDeLinhas = 0;
        // Definir o primeiro dia do mês
        var mesImpresso = (mes < 10) ? "0" + mes : mes;

        var corpoTabela = document.createElement('tbody');
        var linhaAtual = document.createElement('tr');

        var diaAno = 0;

        var primeiroDia = new Date(ano, mes - 1, 1).getDay();
        var ultimoDia = new Date(ano, mes, 0).getDay();
        // Gerar as linhas da tabela
        var comecou = false;

        for (var i = 0; i < new Date(ano, mes, 0).getDate(); i++) {

            diaAno++;

            // Gerar células vazias para os dias antes do primeiro dia do mês
            for (var j = 0; j < primeiroDia; j++) {
                if (!comecou) {
                    linhaAtual.appendChild(document.createElement('td'));
                }
            }
            comecou = true;

            // Gerar a célula para o dia atual
            var dia = i + 1;
            var novoDiaAno = new Date(ano, mes - 1, dia);
            // var novoDiaAnoImpresso = novoDiaAno.getTime() / 1000;
            var novoDiaAnoImpresso = novoDiaAno.getTime();

            var celula = document.createElement('td');
            celula.id = novoDiaAnoImpresso;
            celula.className = 'dia';
            celula.textContent = novoDiaAno.getDate();

            linhaAtual.appendChild(celula);

            // Gerar células vazias para os dias após o último dia do mês
            var diasNoMes = new Date(ano, mes, 0).getDate();
            for (var k = primeiroDia + diasNoMes; k < 7; k++) {
                linhaAtual.appendChild(document.createElement('td'));
            }

            // Se for o último dia da semana, finalizar a linha
            if (primeiroDia === 6) {
                corpoTabela.appendChild(linhaAtual);
                linhaAtual = document.createElement('tr');
                contadorDeLinhas += 1;
                if (dia !== ultimoDia) {
                    linhaAtual = document.createElement('tr');
                    contadorDeLinhas += 1;
                    // echo '<tr class="m' . ($mesImpresso) . '">';
                }
                comecou = false;
            }

            // Incrementar o primeiro dia da semana para o próximo mês
            primeiroDia = (primeiroDia + 1) % 7;
        }
        if (corpoTabela.children.length < contadorDeLinhas) {
            corpoTabela.appendChild(linhaAtual);
        }
        // Adicionar as linhas geradas à tabela existente
        tabela.appendChild(corpoTabela);
    }

    // Função para gerar uma tabela para um mês específico
    function gerarTabela(mes, ano) {
        var divTabela = document.createElement('div');
        divTabela.className = 'div-tabela';

        var nomeMes = document.createElement('h2');
        nomeMes.className = 'nomeMes';
        nomeMes.textContent = new Date(ano, mes - 1, 1).toLocaleString('default', {month: 'long'}) + ' ' + ano;

        var tabela = document.createElement('table');
        tabela.className = 'tabela';
        tabela.id = 'm' + mes;

        gerarCabecalho(tabela);
        gerarCorpo(tabela, mes, ano);

        divTabela.appendChild(nomeMes);
        divTabela.appendChild(tabela);

        tabelasContainer.appendChild(divTabela);
    }

    // Gerar tabelas para todos os meses de 2024
    for (var mes = 1; mes <= 12; mes++) {
        gerarTabela(mes, 2024);
    }


    const tds = document.querySelectorAll('.dia');
// Adiciona evento de clique a cada TD
    tds.forEach(td => {
        td.addEventListener('click', function (evento) {
            debugger
            if (evento.altKey && !this.classList.contains(TIPO_DATA.diaMarcado.afastamento.classeCss)) {
                handleAltKey(this.id, "afastamento", TIPO_DATA.diaMarcado.afastamento.idContornoInicial);

            } else if (evento.ctrlKey && !this.classList.contains(TIPO_DATA.diaMarcado.substituicao.classeCss)) {
                handleCtrlKey(this.id, "substituicao", TIPO_DATA.diaMarcado.substituicao.idContornoInicial);

            } else {
                if (TIPO_DATA.diaMarcado.afastamento.idContornoInicial !== null) {
                    limpar(TIPO_DATA.diaMarcado.afastamento.idContornoInicial, "afastamento")
                    TIPO_DATA.diaMarcado.afastamento.idContornoInicial = null;
                }
                if (TIPO_DATA.diaMarcado.substituicao.idContornoInicial !== null) {
                    limpar(TIPO_DATA.diaMarcado.substituicao.idContornoInicial, "substituicao")
                    TIPO_DATA.diaMarcado.substituicao.idContornoInicial = null;
                }
                if (this.classList.contains('marcado')) {

                    apagaAlvo(this.id);

                }
            }
        });
    });


});


// Seleciona todos os TDs da tabela


function handleAltKey(tdId, tipoDia, inicial) {
    if (inicial && inicial <= tdId) {
        marcarDatas(inicial, tdId, tipoDia);
    } else if (inicial == null) {
        contornarInicio(tdId, tipoDia);
    }
}

function handleCtrlKey(tdId, tipoDia, inicial) {
    if (inicial && inicial <= tdId) {
        marcarDatas(inicial, tdId, tipoDia);
    } else if (inicial == null) {
        contornarInicio(tdId, tipoDia);
    }
}

function apagarContorno(id, tipoData) {
    document.getElementById(id).classList.remove(TIPO_DATA.diaMarcado[tipoData].classeCssInicio);
}

function limpar(id, tipoData) {

    if (id === null) {
        return;
    }
    for (const dia of Object.values(TIPO_DATA.diaMarcado[tipoData])) {
        const classlist = document.getElementById(id).classList;
        if (classlist.contains(dia.classeCssInicio)) document.getElementById(id).classList.remove(dia.classeCssInicio);
        if (classlist.contains(dia.classeCss)) document.getElementById(id).classList.remove(dia.classeCss);
    }
    TIPO_DATA.diaMarcado[tipoData].apagaAlvo(id)
}

function apagaAlvo(id) {
    if (id === null) {
        return;
    }
    for (const dia of Object.values(TIPO_DATA.diaMarcado)) {
        document.getElementById(id).classList.remove(dia.classeCssInicio);
        document.getElementById(id).classList.remove(dia.classeCss);
    }


}

function contornarInicio(id, tipo) {
    TIPO_DATA.diaMarcado[tipo].idContornoInicial = id;
    document.getElementById(id).classList.add(TIPO_DATA.diaMarcado[tipo].classeCssInicio);
}


function contornarInicioSubst(id) {
    document.getElementById(id).classList.add('tdDiaInicioSubstituicao');
}


function checaConflitos(id, tipoData) {
    if (TIPO_DATA.diaMarcado.substituicao.arrayIdDias.includes(id) || TIPO_DATA.diaMarcado.afastamento.arrayIdDias.includes(id)) {
        TIPO_DATA.diaMarcado.conflito.arrayIdDias.push(id);
        return true;
    } else {
        TIPO_DATA.diaMarcado[tipoData].arrayIdDias.push(id);
        return false;
    }
}

function marcarDatasConflitos() {

    const substituicoesMarcadas = TIPO_DATA.diaMarcado.substituicao.paresDeDatas
    const afastamentosMarcados = TIPO_DATA.diaMarcado.substituicao.paresDeDatas

    for (const afastamentoMarcado of afastamentosMarcados) {
        const arrayDeIdsDeAfastamentos = afastamentoMarcado.arrayDeIdsDessePar;
    }


}

function marcarDatas(idInicio, idFim, tipoData) {


    apagarContorno(idInicio, tipoData);

    let entrouEmConflito = false;
    let primeiroDoLoopDeConflito = true;
    let diaAlvo = parseInt(idInicio);
    let pintados = [];

    let datasPraRegistro = [];

    datasPraRegistro[0] = {inicio: diaAlvo, fim: diaAlvo, situacao: tipoData};


    const totalDeDias = (idFim - idInicio) / (MILISEGUNDOS_EM_UM_DIA)
    for (let i = 0; i <= totalDeDias; i++) {
        // datasPraRegistro[datasPraRegistro.length - 1].fim = diaAlvo;
        // if (!document.getElementById(diaAlvo.toString()).classList.contains(TIPO_DATA.diaMarcado.conflito.classeCss)) {

        if (checaConflitos(diaAlvo, tipoData)) {
            // em caso de emergencia apagar bloco abaixo
            entrouEmConflito = true;

            if (primeiroDoLoopDeConflito) {
                primeiroDoLoopDeConflito = false;
                if (parseInt(idInicio) !== diaAlvo) {
                    datasPraRegistro.push({inicio: diaAlvo, situacao: "conflito"})

                }
            }
            datasPraRegistro[datasPraRegistro.length - 1].fim = diaAlvo;
        } else {
            if (entrouEmConflito && !primeiroDoLoopDeConflito) {
                primeiroDoLoopDeConflito = true;
                datasPraRegistro.push({inicio: diaAlvo, situacao: tipoData})
            }
            datasPraRegistro[datasPraRegistro.length - 1].fim = diaAlvo;
            entrouEmConflito = false;
            document.getElementById(diaAlvo.toString()).classList.add(TIPO_DATA.diaMarcado[tipoData].classeCss);
        }

        document.getElementById(diaAlvo.toString()).classList.add('marcado');
        diaAlvo += MILISEGUNDOS_EM_UM_DIA;

    }

    TIPO_DATA.diaMarcado[tipoData].idContornoInicial = null;

    for (const datas of datasPraRegistro) {
        criarDataParaRelatorio(datas["inicio"], datas["fim"], datas.situacao)

    }


}


function criarDataParaRelatorio(idDiaInicio, idDiaFim, tipoData) {

    const datas = {
        data1: criaObjetoData(idDiaInicio), data2: criaObjetoData(idDiaFim)
    }

    const dataLegivel = `${datas.data1["dia"]}/${datas.data1["mes"]}/${datas.data1["ano"]}`;
    const dataLegivel2 = `${datas.data2["dia"]}/${datas.data2["mes"]}/${datas.data2["ano"]}`;
    const elementoPai = document.getElementById(TIPO_DATA.diaMarcado[tipoData].divRelatorioPai);
    const idNaLista = `${TIPO_DATA.diaMarcado[tipoData].divRelatorio}${elementoPai.children.length + 1}`;

    TIPO_DATA.diaMarcado[tipoData].paresDeDatas.push({
        "diaInicio": dataLegivel, "diaFim": dataLegivel2, arrayDeIdsDessePar: TIPO_DATA.diaMarcado[tipoData].arrayIdDias, idNaLista: idNaLista
    })

    TIPO_DATA.diaMarcado[tipoData].arrayIdDias = []

    criaLiData(dataLegivel, dataLegivel2, tipoData, idNaLista)

}

function criaLiData(data1, data2, tipoData, idNaLista) {
    const elementoPai = document.getElementById(TIPO_DATA.diaMarcado[tipoData].divRelatorioPai);

    const liElement = document.createElement("li");
    liElement.id = idNaLista;
    liElement.classList.add(TIPO_DATA.diaMarcado[tipoData].divRelatorio);
    liElement.addEventListener('pointerenter', function () {
        apontarDataDestacaCalendario(TIPO_DATA.diaMarcado[tipoData].paresDeDatas.find(item => item.idNaLista === this.id).arrayDeIdsDessePar, tipoData)
    })
    liElement.addEventListener('pointerleave', function () {
        tirarPointerRemoveDestaque(TIPO_DATA.diaMarcado[tipoData].paresDeDatas.find(item => item.idNaLista === this.id).arrayDeIdsDessePar, tipoData)
    })
    const spanInicio = document.createElement("span");
    spanInicio.classList.add("inicio");
    spanInicio.textContent = data1;
    spanInicio.addEventListener('click', function () {
        navigator.clipboard.writeText(this.innerText).then(() => {
            console.log("Texto copiado para a �rea de transfer�ncia!");
        }, (err) => {
            console.error("Erro ao copiar texto:", err);
        });

    })

    const spanFim = document.createElement("span");
    spanFim.classList.add("fim");
    spanFim.textContent = data2;
    spanFim.addEventListener('click', function () {
        navigator.clipboard.writeText(this.innerText).then(() => {
            console.log("Texto copiado para a �rea de transfer�ncia!");
        }, (err) => {
            console.error("Erro ao copiar texto:", err);
        });
    })

    liElement.appendChild(spanInicio);
    liElement.append("  ->  ");
    liElement.appendChild(spanFim);

    elementoPai.appendChild(liElement);


}

function criaObjetoData(idDia) {

    const objetoData = new Date((idDia) );

    const dia = objetoData.getDate();
    const mes = objetoData.getMonth() + 1;
    const ano = objetoData.getFullYear();

    return {
        "dia": dia < 10 ? '0' + dia : dia, "mes": mes < 10 ? '0' + mes : mes, "ano": ano, toString: `${dia < 10 ? '0' + dia : dia}/${mes < 10 ? '0' + mes : mes}/${ano}`
    }

}


function resolverConflitosDatasImpressas() {


}

const botaoResolverConflitos = document.getElementById('resolver-conflitos');
const botaoResolverConflitosCalendario = document.getElementById('resolver-conflitos-calendario');
const botaoLimparTela = document.getElementById('limparTela');

botaoResolverConflitosCalendario.addEventListener('click', () => {
    marcarDatasConflitos();
});

botaoResolverConflitos.addEventListener('click', () => {
    resolverConflitosDatasImpressas();
});
botaoLimparTela.addEventListener('click', () => {
    for (const tipoDia in TIPO_DATA.diaMarcado) {
        document.getElementById(TIPO_DATA.diaMarcado[tipoDia].divRelatorioPai).innerHTML = "";
        for (const tipoDiaElement of TIPO_DATA.diaMarcado[tipoDia].paresDeDatas) {

            tipoDiaElement.arrayDeIdsDessePar.map(value => {
                document.getElementById(value).classList.remove(TIPO_DATA.diaMarcado[tipoDia].classeCss);
                document.getElementById(value).classList.remove('marcado');
            })
        }
        limpar(TIPO_DATA.diaMarcado[tipoDia].idContornoInicial, tipoDia)
        TIPO_DATA.diaMarcado[tipoDia].paresDeDatas = []
        TIPO_DATA.diaMarcado[tipoDia].arrayIdDias = [];
    }
});


function apontarDataDestacaCalendario(arrayDeIds, tipoData) {
    for (const idDoTd of arrayDeIds) {
        document.getElementById(idDoTd).classList.add(TIPO_DATA.diaMarcado[tipoData].classeCssDestaque)
    }
}

function tirarPointerRemoveDestaque(arrayDeIds, tipoData) {
    for (const idDoTd of arrayDeIds) {
        document.getElementById(idDoTd).classList.remove(TIPO_DATA.diaMarcado[tipoData].classeCssDestaque)
    }
}
